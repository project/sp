Sendspace API (beta) PHP class & example
========================================

sendspace_api.class.php - A PHP class fully implementing the sendspace API.

client_sample_full.php - A sample usage of the class and the options available.

progress_proxy.php - Used for proxying xml progress bar requests.

To start testing or using this package set the API key define (SENDSPACE_API_KEY) in client_sample_full.php to the key provided to you through the key creation process in the Developer section.

The code was written for PHP5 and requires the curl PHP module.

For more information see:
http://www.sendspace.com/developers/
